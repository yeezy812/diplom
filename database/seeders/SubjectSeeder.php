<?php

namespace Database\Seeders;

use App\Models\Subject;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class SubjectSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $values = ['математика','русский язык', 'литература', 'физика', 'история',' обществознание'];
        $data=[];
        foreach ($values as $value){
            $data[] = [
                'name' => $value
            ];
        }
         Subject::insert($data);
    }
}
