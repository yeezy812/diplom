<?php

namespace Database\Seeders;

use App\Models\Education;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class EducationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $values = ['Высшее','Среднее профессиональное', 'Среднее общее', 'Школьник'];
        $data=[];
        foreach ($values as $value){
            $data[] = [
                'name' => $value
            ];
        }
        Education::insert($data);
    }
}
