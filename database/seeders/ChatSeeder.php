<?php

namespace Database\Seeders;

use App\Models\ChatMessage;
use App\Models\ChatRoom;
use App\Models\ChatRoomUser;
use App\Models\User;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class ChatSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       $users = User::select(['id'])->get();
       $usersLength = count($users);
        $rooms =\App\Models\ChatRoom::factory($usersLength - 1)->create();
        $prepareArray = [];
       for ($i = 1; $i < $usersLength; $i++){
         $prepareArray[] =[
             'chat_room_id' => $rooms[$i - 1]->id,
             'user_id' => $users[$i]->id
         ];
           $prepareArray[] =[
               'chat_room_id' => $rooms[$i - 1]->id,
               'user_id' => $users[0]->id
           ];
       }
       ChatRoomUser::insert($prepareArray);
    }
}
