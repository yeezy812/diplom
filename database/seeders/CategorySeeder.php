<?php

namespace Database\Seeders;

use App\Models\Category;
use App\Models\Subject;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $values = ['1-4 класс','5-9 класс', '10-11 класс', 'студенты'];
        $data=[];
        $subjects = Subject::all();
        foreach ($values as $value){
           $category =  Category::create(['name' => $value]);
           $category->subjects()->attach($subjects);
        }
    }
}
