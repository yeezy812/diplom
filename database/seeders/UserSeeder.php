<?php

namespace Database\Seeders;

use App\Models\User;
use Faker\Factory;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use App\Models;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      $parents = Models\ParentRole::factory(3)->create();


        //creating teachers
        Models\TeacherRole::factory(30)->create()->each(function ($teacher)  {
          $ids =  Models\CategorySubject::inRandomOrder()->limit(4)->get();
          $data = [];
            foreach ($ids as $id){
                //     print_r($id);
                $data[$id->id] = ["price" => rand(100, 1000)];
            }
            $teacher->pivotToSubjects()->sync($data);

            $teacher->save();
        });

       // creating children
        User::factory(9)->create()->each(function ($user) use ($parents) {
            $id =
                Models\StudentRole::create(
                    ['user_id' => $user->id ,
                        'parent_id' => Factory::create()->randomElement($parents)->id
                    ]);
        });
    }
}
