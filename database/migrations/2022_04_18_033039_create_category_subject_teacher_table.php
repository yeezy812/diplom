<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('category_subject_teacher', function (Blueprint $table) {
//            $table->id();
            $table->foreignId('teacher_id')->constrained()->onDelete('cascade');
            $table->foreignId('category_subject_id')->constrained('category_subject')->onDelete('cascade');
            $table->integer('price')->unsigned()->nullable();
            $table->primary(['teacher_id', 'category_subject_id'], 'category_subject_teacher_primary');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('category_subject_teacher');
    }
};
