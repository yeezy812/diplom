<?php

namespace Database\Factories;

use App\Models\Education;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends Factory
 */
class TeacherRoleFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        $user = User::factory()->create();
        return [
            'user_id' =>  $user->id,
            'education_id' => Education::inRandomOrder()->first()->id,
            'about' => $this->faker->paragraph(),
            'keywords' => $this->faker->sentence(),
            'work_experience' => $this->faker->numberBetween(0,70)
        ];
    }
}
