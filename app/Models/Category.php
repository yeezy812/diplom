<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    use HasFactory;


    public function subjects()
    {
        return $this->belongsToMany(Subject::class,'category_subject','category_id')->withPivot('id');
    }

    public function idsOfSubjects(){
        $ids=[];
        foreach ($this->subjects as $subject){
            $ids[] = $subject->pivot->id;
        }
        return $ids;
    }
}
