<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class TeacherRole extends Model
{
    use HasFactory;
    use \Staudenmeir\EloquentHasManyDeep\HasRelationships;

    protected $table = 'teachers';


    protected $fillable = ['user_id', 'education_id', 'work_experience', 'about', 'keywords'];

    public function user(){
        return $this->belongsTo(User::class);
    }

    public function education(){
        return $this->belongsTo(Education::class);
    }

    public function teacherReviews(){
        return $this->hasMany(TeacherReview::class, 'teacher_id')->with(['author' => function ($query) {
            $query->select('id', 'name', 'last_name');
        }]);
    }

    public function getReviewsCount(){
        $reviewsCount =  $this->teacherReviews()->count();
        $remainder =  $reviewsCount % 10;
        $reviewsName = 'отзыв';
        if ($reviewsCount % 100 >= 11 && $reviewsCount <= 14)
            $reviewsName.= 'ов';
        elseif ($remainder == 0 || ($remainder >= 5 && $remainder <= 9))
         $reviewsName .= 'ов';
        elseif ($remainder >= 2 && $remainder <= 4)
            $reviewsName .= 'а';
        return $reviewsCount.' '.$reviewsName;
    }

    public function subjectIds(){
        $pivot =  $this->pivotToSubjects;
        $ids=[];
        foreach ($pivot as $elem){
            $ids[]=$elem->id;
        }
        return $ids;
    }


    public function getPrices():array
    {
        $results = \App\Models\CategorySubjectTeacher::
        where('teacher_id',Auth::user()->teacherRole->id)
            ->select('price', 'category_subject_id')
            ->get();
        $prices =[ ];

        foreach ($results as $result){
            $prices[$result->category_subject_id] = $result->price;
        }
        return $prices;
    }


    public function pivotToSubjects()
    {
        return  $this
            ->belongsToMany(CategorySubject::class, 'category_subject_teacher', 'teacher_id')
            ->withPivot('price')->with(['subject', 'category']); //->with('category');
     //  return $this->hasMany(CategorySubjectTeacher::class,'teacher_id')->with('subjects');
    }


    public function getAllCategoriesAndSubjects(): array
    {
        $test=[];
        $teacherSubjects = $this->pivotToSubjects;
        foreach ($teacherSubjects as $teacherSubject){
            $test[$teacherSubject->category->id]['name'] = $teacherSubject->category->name;
            $test[$teacherSubject->category->id]['attributes'][$teacherSubject->subject->id] =[
                'price' => $teacherSubject->pivot->price,
                'name' => $teacherSubject->subject->name
            ];
        }
   //     print_r($test);
        return $test;
    }





}
