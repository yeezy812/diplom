<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TeacherReview extends Model
{
    use HasFactory;

    protected $fillable = ['score', 'comment'];

    public function author(){
       return $this->belongsTo(User::class, 'user_id');
    }

    public function teacher(){
        return $this->belongsTo(TeacherRole::class, 'teacher_id');
    }
}
