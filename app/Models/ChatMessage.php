<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ChatMessage extends Model
{
    use HasFactory;

    public function user(){
        return $this->belongsTo(User::class)->select(['id', 'name', 'last_name', 'middle_name']);
    }


}
