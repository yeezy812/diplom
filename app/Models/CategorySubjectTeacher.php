<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CategorySubjectTeacher extends Model
{
    use HasFactory;

    use \Znck\Eloquent\Traits\BelongsToThrough;

    protected $table = 'category_subject_teacher';

    protected $fillable = ['teacher_id', 'category_subject_id', 'price'];


}
