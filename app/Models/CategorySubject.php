<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CategorySubject extends Model
{
    use HasFactory;

    protected $table = 'category_subject';

    public function subject(){
        return $this->belongsTo(Subject::class);
    }

    public function category(){
        return $this->belongsTo(Category::class);
    }


}
