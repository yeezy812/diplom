<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class ChatRoom extends Model
{
    use HasFactory;

    public function messages(){
      //  print_r("eeeeeee");
        return $this->hasMany(ChatMessage::class,'chat_room_id');
    }

    public function anotherUser(){
        return $this->belongsToMany(User::class)->where('user_id', '!=', Auth::user()->id)
            ->select('id', 'name', 'last_name');
    }


    public function lastMessage(){
        return $this->hasOne(ChatMessage::class,'chat_room_id')->with('user')->latest();
    }

    public function users(){
        return $this->belongsToMany(User::class);
    }
}
