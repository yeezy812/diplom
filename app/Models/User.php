<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable;
    use \Staudenmeir\EloquentHasManyDeep\HasRelationships;
    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'login',
        'name',
        'last_name',
        'middle_name',
        'email',
        'password',
        'birthdate',
        'image_path'
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];


    /**
     * Creates teacher role
     *
     * @return User
     */
    public function becomeTeacher(): User
    {
        if (!$this->isTeacher())
        {
            TeacherRole::create(["user_id" => $this->id]);
        }
        return $this;
    }

    /**
     * Creates parent role
     *
     * @return User
     */
    public function becomeParent() : User
    {
        if (!$this->isParent() && !$this->isStudent())
        {
            ParentRole::create(["user_id" => $this->id]);
        }
        return $this;
    }

    public function parentRole(){
      return  $this->hasOne(ParentRole::class);
    }

    public function studentRole(){
      return  $this->hasOne(StudentRole::class);
    }


    public function teacherRole(){
       return $this->hasOne(TeacherRole::class);
    }

    public function setPasswordAttributes($password){
        $this->attributes["password"] = Hash::make($password);
    }


    public function isTeacher() : bool
    {
        return (bool)$this->teacherRole;
    }

    public function isParent() : bool
    {
        return (bool)$this->parentRole;
    }


    public function isStudent() : bool
    {
        return (bool)$this->studentRole;
    }

    public function role() :string
    {
        $roleName = '';
        if ($this->isStudent())
            $roleName = "Ученик";
        if ($this->isParent())
            $roleName = "Родитель";
        if ($this->isTeacher()) {
            if ($roleName != '')
                $roleName .= ", ";
            $roleName .= "Преподаватель";
        }
            return  $roleName;
    }


    public function getProfilePicturePath() : string
    {
        if (isset($this->image_path))
            return asset('images/'.$this->image_path);
        else
            return asset('images/default_user_photo.png');
    }



    public function children()
    {
        return $this->hasManyDeep(
            User::class,
            [ParentRole::class, StudentRole::class ],
            [null, 'parent_id' ,'id'],
            [null, 'id', 'user_id'],
            [null, ['taggable_type', 'taggable_id'], 'user_id']
        );
    }

    public function reviews(){
        return $this->hasMany(TeacherReview::class,'user_id');
    }

    public function rooms(){
        return $this->belongsToMany(ChatRoom::class,);
    }

}
