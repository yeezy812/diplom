<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class StudentRole extends Model
{
    use HasFactory;
    protected $table = 'students';

    protected  $fillable = ['user_id', 'parent_id'];

    public function userParent(){
        return  $this->belongsTo(ParentRole::class, 'parent_id');
    }

}
