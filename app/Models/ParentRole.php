<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ParentRole extends Model
{
    use HasFactory;

    protected $table = 'parents';

    protected  $fillable = ['user_id'];

    public function children()
    {
        return $this->hasMany(User::class);
    }

    public function user(){
        return $this->belongsTo(User::class);
    }



}
