<?php

namespace App\Http\Controllers;

use App\Http\Requests\RegistrationRequest;
use App\Models\TeacherReview;
use App\Models\TeacherRole;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Intervention\Image\Facades\Image;

class UserController extends Controller
{

    public function setUserPhoto(Request $request)
    {
        $user = Auth::user();
        $request->validate([
            'profile_image' => 'mimes:jpg,png,jpeg|max:5048'
        ]);
        $newImageName = time() . '-' . $user->login . '-profile-photo.' . $request->profile_image->extension();
        $imageSize = getimagesize($request->profile_image);
        $min = min($imageSize[0],$imageSize[1]);
        $request->profile_image->move(public_path('images'), $newImageName);
        $resizedImage=Image::make(public_path('images').'/'.$newImageName)->fit($min,$min)->save();
        $oldImage = $user->image_path;
        $user->update(['image_path' => $newImageName]);
        if (file_exists(public_path('images') . '/' . $oldImage) && $oldImage)
            unlink(public_path('images') . '/' . $oldImage);
        return back()->with('image_updated', 'Успешно обновлено');

    }



    public function updateInfo(Request $request): \Illuminate\Http\RedirectResponse
    {
        $validated = $request->validate([
            'last_name' => 'string|min:1|max:30',
            'name' => 'string|min:1|max:30',
            'middle_name' => 'nullable|string|min:1|max:30',
            'birthdate' => 'date',
        ]);
        $validated["birthdate"] = date('Y-m-d', strtotime( $validated["birthdate"] ));
       Auth::user()->update($validated);
        return redirect()->back()->with('passport-data.success', 'Данные успешно обновлены');
    }

    public function updatePassword(Request $request)
    {
        $validated = $request->validate([
            'current_password' => 'required',
            'new_password' => 'required|confirmed|min:6',
        ]);
        $user = Auth::user();
        if (Hash::check($request->current_password, $user->password)) {
            $user->update(['password' => Hash::make($request->new_password)]);
            return redirect()->back()->with('password.success', 'Пароль обновлен');
        }
        return redirect()->back()->withErrors(['current_password' => 'неверный пароль']);


    }

    public function becomeTeacher(){
         Auth::user()->becomeTeacher();
        return redirect()->back();
    }

    public function becomeParent(){
         Auth::user()->becomeParent();
        return redirect()->back();
    }


    public function addReview(Request $request,int $id){
        $validated = $request->validate([
            'comment' => 'required|string',
            'score' => 'required|integer|between:1,5',
            ]);
        $review = new TeacherReview();
        $review->comment = $validated['comment'];
        $review->score = $validated["score"];
        $review->teacher()->associate(TeacherRole::where('user_id', $id)->firstOrFail());
        $review->author()->associate(Auth::user());
        $review->save();
        return redirect()->back();
    }


    public function updateEmail(Request $request){
        $validated= $request->validate([
            'email' => 'email|unique:users'
        ]);
        $user = Auth::user();
        $user->update($validated);
        return redirect()->back()->with('email.success', 'Почта успешно обновлена');;
    }



}
