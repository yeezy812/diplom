<?php

namespace App\Http\Controllers;

use App\Models\Degree;
use App\Models\TeacherRole;
use App\Models\User;
use Illuminate\Http\Request;

class SearchController extends Controller
{
    public function findKeyWords($q, $teachers)
    {
        $words = [];
        $q = htmlspecialchars($q);
        $q = str_replace(['-', ';', ',', '.', '|'], ' ', $q);

        $q = preg_replace('/\s+/', ' ', $q);
        if (strlen($q) > 0) {
            $words = explode(' ', $q);

            return $teachers
                ->where(function ($query) use ($words) {
                    foreach ($words as $word) {
                        $query->orWhere('about', 'like', '%' . $word . '%')
                            ->orWhere('keywords', 'like', '%' . $word . '%');
                    }
                })
//                ->orWhereHas('education', function ($query) use ($words) {
//                    $query->where(function ($query) use ($words) {
//                        foreach ($words as $word) {
//                            $query->orWhere('educations.name', 'like', '%' . $word . '%');
//                        }
//                    });
//                })
//                ->orWhereHas('pivotToSubjects', function ($query) use ($words) {
//                    $query->whereHas('subject', function ($query) use ($words) {
//                        $query->where(function ($query) use ($words) {
//                            foreach ($words as $word) {
//                                $query->orWhere('subjects.name', 'like', '%' . $word . '%');
//                            }
//                        });
//
//                    });
//                });
                ;
        }
        return $teachers;
    }


    public function show(Request $request)
    {
//       $validated  =  $request->validate([
//            'min_work_experience' => 'integer',
//          'education' => 'nullable|string'
//        ]);
      //    dd($request->all());

        $teachers = TeacherRole::with('user')->with('education');

        $teachers = $this->findKeyWords($request->q, $teachers);


        $minPrice = $request->min_price;
        $maxPrice = $request->max_price;
        $categoryId = $request->category;
        $subjectId = $request->subject;

        $teachers = $teachers->whereHas('pivotToSubjects', function ($query) use ($categoryId, $subjectId, $minPrice, $maxPrice) {
            $query->when($categoryId > 0, function ($query) use ($categoryId) {
                $query->where('category_id', $categoryId);
            })->when($subjectId > 0, function ($query) use ($subjectId) {
                    $query->where('subject_id', $subjectId);
                })->when($minPrice > 0, function ($query) use ($minPrice) {
                    $query->where('price', '>=', $minPrice);
                })
                ->when($maxPrice > 0, function ($query) use ($maxPrice) {

                    $query->where('price', '<=', $maxPrice);
                });
        });
        if ($request->education) {
            $educationId = $request->education;
            $teachers = $teachers->whereHas('education', function ($query) use ($educationId) {
                $query->where('id', $educationId);
            });
        }

        if ($request->min_work_experience) {
            $teachers = $teachers->where('work_experience', '>=', $request->min_work_experience);
        }

        switch($request->order){
            case 'reviews_count':  $teachers = $teachers->withCount('teacherReviews')->orderBy('teacher_reviews_count', 'desc'); break;
            case 'reviews_avg' : $teachers = $teachers->withAvg('teacherReviews','score')->orderBy('teacher_reviews_avg_score', 'desc'); break;
            case 'created_at' : $teachers = $teachers->orderBy('created_at', 'desc');
          //  case 'price_asc':  $teachers = $teachers->orderBy('price'); break;
        }
    //    print_r($teachers->toSql());
        return view('search', ['teachers' => $teachers->paginate(10)->appends($request->all()), 'request' => $request->toArray()]);
    }
}
