<?php

namespace App\Http\Controllers;

use App\Models\TeacherReview;
use App\Models\TeacherRole;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class TeacherReviewController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, int $id)
    {
        $validated = $request->validate([
            'comment' => 'required|string',
            'score' => 'required|integer|between:1,5',
        ]);
        $review = new TeacherReview();
        $review->comment = $validated['comment'];
        $review->score = $validated["score"];

        $review->teacher()->associate(TeacherRole::findOrFail($id));
        $review->author()->associate(Auth::user());
        $review->save();
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validated = $request->validate([
            'comment' => 'required|string',
            'score' => 'required|integer|between:1,5',
        ]);
        $review = Auth::user()->reviews()->findOrFail($id);
        $review->update( $validated);
//        $review->score = $validated["score"];
//        $review->teacher()->associate(TeacherRole::where('user_id', $id)->firstOrFail());
//        $review->author()->associate(Auth::user());
//        $review->save();
        return redirect()->back();

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = Auth::user();
       $user->reviews()->findOrFail($id)->delete();
       return redirect()->back();
    }
}
