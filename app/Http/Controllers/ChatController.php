<?php

namespace App\Http\Controllers;

use App\Events\NewCurrentChatMessage;
use App\Models\ChatMessage;
use App\Models\ChatRoom;
use App\Models\TeacherRole;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ChatController extends Controller
{
    public function index( $id = null){
        $rooms = Auth::user()->rooms()->with('lastMessage')->get();

        if ($id){
            $room = Auth::user()->rooms()->with('anotherUser')->findOrFail($id);
            return view('test', ['room' => $room, 'rooms' => $rooms]);
        }
     return view('test', [ 'rooms' => $rooms]);
    }


    public function rooms(Request $request){
        return Auth::user()->rooms()->with('anotherUser')
            ->join('chat_messages', 'chat_messages.chat_room_id', '=', 'chat_rooms.id')
            ->select('chat_rooms.*', 'chat_messages.created_at AS latest_message_date', 'chat_messages.message AS latest_message')
            ->whereNotExists(function ($subquery) {
                return $subquery->from('chat_messages AS later_messages')
                    ->whereRaw('later_messages.chat_room_id = chat_messages.chat_room_id')
                    ->whereRaw('later_messages.created_at > chat_messages.created_at')
              ;
            })
            ->orderByDesc('latest_message_date')
            ->paginate(20);
//        ->join('chat_messages', 'chat_messages.chat_room_id', '=', 'chat_rooms.id')
//            ->join('users', 'users.id', '=', 'chat_room_user.user_id')
//            ->select('chat_rooms.*', 'chat_messages.created_at AS latest_message_date',
//                'chat_messages.message AS latest_message', 'users.name', 'users.last_name')
//            ->whereNotExists(function ($subquery) {
//                return $subquery->from('chat_messages AS later_messages')
//                    ->whereRaw('later_messages.chat_room_id = chat_messages.chat_room_id')
//                    ->whereRaw('later_messages.created_at > chat_messages.created_at')
//                    ;
//            })
//            ->orderByDesc('latest_message_date')
//            ->paginate(20);
    }

    public function messages(Request $request, $id){
        return ChatMessage::where('chat_room_id', $id)
            ->with('user')
            ->orderBy('created_at', 'desc')
            ->paginate(20);
    }

    public function store(Request $request, $id)
    {
        $room = Auth::user()->rooms()->with('messages')->findOrFail($id);
        $message =new ChatMessage();
        $message->user_id= Auth::user()->id;
        $message->chat_room_id = $room->id;
        $message->message = $request->message;
        $message->save();
      //  print_r($message->with('user')->find($message->id)->toArray());
       $message = $message->with('user')->find($message->id);
        broadcast(new NewCurrentChatMessage($message))->toOthers();
        return $message;
    }

    public function openChat($id)
    {

        $teacherUser = TeacherRole::findOrFail($id)->user;
        print_r(Auth::user()->id);
        print_r($teacherUser->id);
        $room =  $teacherUser->rooms()
            ->whereHas('users', function ($q) use($teacherUser){
            $q->where('user_id', '=', Auth::user()->id);
        })
            ->whereHas('users', function ($q) use($teacherUser){
                $q->where('user_id', '=', $teacherUser->id);
            })
            ->firstOr(function () use($teacherUser){
                print_r("eeef");
              $newRoom = ChatRoom::create();
              $newRoom->users()->sync([$teacherUser->id, Auth::user()->id]);
              $newRoom->save();
              return $newRoom;
            });

      return  redirect()->route('user.chat-room', ['id' => $room->id]);
    }
}
