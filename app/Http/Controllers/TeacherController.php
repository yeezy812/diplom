<?php

namespace App\Http\Controllers;

use App\Models\CategorySubject;
use App\Models\CategorySubjectTeacher;
use App\Models\TeacherRole;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class TeacherController extends Controller
{
    public function update(Request $request){
        $user = Auth::user();
        if ($user->isTeacher()) {
            if ($request->education_id == '')
                $request->education_id = null;

            $request->validate(
                ['education_id' => 'nullable|integer',
                    'keywords' => 'nullable|string',
                    'about' => 'nullable|string',
                    'work_experience' => 'nullable|string',
                    'selected_subjects' => 'string',
                    'prices' => 'array'
                ]);
            $subjects = collect($request->input('prices', []))
                ->map(function ($price){
                    return ['price' => $price];
                });
            $user->teacherRole
                -> pivotToSubjects()
                ->sync( $subjects);
            $user->teacherRole->update($request->except('selected_subjects'));

            return redirect()->back()->with('teacher.success', 'данные успешно обновлены');
        }
        return redirect()->back();
    }



}
