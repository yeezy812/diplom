<?php

namespace App\Http\Controllers;

use App\Models\TeacherRole;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class TeacherPageController extends Controller
{
    public function index($id){
        $teacher = TeacherRole::findOrFail($id);
        $review = [];
        $route = '';
        if (\Illuminate\Support\Facades\Auth::check()){

            $review =$teacher->teacherReviews() ->where('user_id', Auth::user()->id)->first();
//            App\Models\TeacherReview::
//            where('teacher_id',\App\Models\TeacherRole::where('user_id',$id)->first()->id)
//                ->where('user_id', Auth::user()->id)->first();

            if ($review) {
                $route = route('user.update-review', ['id' => $review['id']]);
            }
            else
            {
                $route = route('user.add-review', ["id" => $teacher->id]);
            }
        }

        $reviews = $teacher->teacherReviews()->paginate(5);
        return view('user-page', ['teacher' =>$teacher, 'reviews'=> $reviews, 'userReview'=>$review, 'route'=> $route ]);
    }
}
