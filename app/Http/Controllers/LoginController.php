<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    public function login(Request $request){
       if (Auth::check()){
           return redirect()->route('/');
       }
        $formFields = $request->only('email', 'password');
       if (Auth::attempt($formFields))
           return redirect()->intended(route('user.profile'));
        return redirect()->back()->with(["error" => " неверная почта или пароль"]);
    }

    public function loginAsChild($id)
    {
        $user = Auth::user();
        if ($user->isParent()) {
            $child = $user->children()->findOrFail($id);
            Auth::login($child);
            return redirect()->intended(route('user.profile'));
        }
        return redirect()->back();
    }

    public function logout(){
        Auth::logout();
        return redirect()->route('user.login');
    }
}
