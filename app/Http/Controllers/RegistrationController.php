<?php

namespace App\Http\Controllers;

use App\Http\Requests\RegistrationRequest;
use App\Models\ParentRole;
use App\Models\StudentRole;
use App\Models\TeacherRole;
use App\Models\User;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class RegistrationController extends Controller
{
    public function defaultRegistration(array $request)
    {
        return User::create([
            "name" => $request["name"],
            "last_name" => $request["last_name"],
            "middle_name" => $request["middle_name"],
            'birthdate' => $request["birthdate"],
            'password' => Hash::make($request["password"]),
            'email' => $request["email"]
        ]);
    }

    public function userRegistration(RegistrationRequest $request)
    {
       $user =  $this->defaultRegistration($request->all());
       if (!isset($request->usertype))
           $request->usertype = 'student';
        switch ($request->usertype) {
            case "teacher" :
                $user->becomeTeacher();
                break;
            case "parent" :
                $user->becomeParent();
                break;
            default :
                break;
        }
        Auth::login($user);
        return redirect()->route('user.profile');
    }


    public function childRegistration(RegistrationRequest $request)
    {
        if (!Auth::user()->isParent())
            return redirect()->route('user.profile');
        $createdUser = $this->defaultRegistration($request->all());
        StudentRole::create(["user_id" =>  $createdUser->id, 'parent_id' => Auth::user()->parentRole->id]);
        return redirect()->route('user.profile')->with(['createdNewUser' => 'Новый аккаунт успешно создан.']);
    }


}
