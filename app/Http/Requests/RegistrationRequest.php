<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RegistrationRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email' => 'required|email|unique:users',
            'password' => 'required|confirmed|min:6',
            'last_name' => 'required',
            'name' => 'required',
            'middle_name' => 'nullable|string',
            'birthdate' => 'date',
            'usertype' => 'nullable|string'
        ];
    }

//
//    /**
//     *
//     * @return array
//     *
//     * */
//    public function messages() : array
//    {
//        return [
//            'login.required' => 'Поле логин - обязательно',
//            'email.required' => 'Поле почта - обязательно',
//            'email.email' => 'Поле почта заполнено неверно',
//            'password.required' => 'Поле пароль - обязательно',
//            'password.confirmed' => 'Пароли не совпадают',
//            'password.min' => 'Минимальная длина пароля 6 символов',
//            'name.required'=>'Поле имя - обязательно',
//            'lastname.required' => 'Поле фамилия - обязательно',
//            'birthdate.required' => 'Поле дата - обязательно',
//            'birthdate.date' => 'Поле дата заполнено неверно'
//        ];
//    }


}
