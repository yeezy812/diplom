<?php

namespace App\Listeners;

use App\Events\NewCurrentChatMessage;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class SendChatMessageNotification
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  \App\Events\NewCurrentChatMessage  $event
     * @return void
     */
    public function handle(NewCurrentChatMessage $event)
    {
        //
    }
}
