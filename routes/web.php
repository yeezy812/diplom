<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use Illuminate\Http\Request;
use App\Http\Controllers\UserController;
use App\Http\Controllers\RegistrationController;
use App\Models\User;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect()->route('search');
});


Route::middleware('auth')->name('user.')->group(function () {
    Route::get('/profile', function () {

        return view('profile', ['user' => Auth::user()]);
    })->name('profile');



    Route::get('/login', function () {
        if (Auth::check())
            return redirect()->route('user.profile');
        return view('login');
    })->withoutMiddleware('auth')->name('login');

    Route::post('/login', [\App\Http\Controllers\LoginController::class, 'login'])
        ->withoutMiddleware('auth');

    Route::get('/logout', [\App\Http\Controllers\LoginController::class, 'logout'])->name('logout');

    Route::get('/registration', function () {
        if (Auth::user())
            return redirect()->route('user.profile');
        return view('registration');
    })->withoutMiddleware('auth')->name('registration');

    Route::post('/registration', [RegistrationController::class, 'userRegistration'])
        ->withoutMiddleware('auth');


    Route::name('update.')->prefix('/profile/update')->group(function(){

        Route::post('/passport-data', [UserController::class, 'updateInfo'])
            ->name('passport-data');

        Route::post('/profile-picture', [UserController::class, 'setUserPhoto'])
            ->name('profile-picture');

        Route::post('/password', [UserController::class, 'updatePassword'])
            ->name('password');

        Route::post('/email', [UserController::class, 'updateEmail'])
            ->name('email');

        Route::post('/teacher-about', [\App\Http\Controllers\TeacherController::class, 'update'])
            ->name('teacher-about');


    });


    Route::get('/profile/register-student', function () {
        if (Auth::user()->isParent())
            return view('registration', ['isCreateStudent' => true]);
        return redirect()->route('user.registration');
    })
        ->name('student-registration');

    Route::post('/profile/register-student', [RegistrationController::class, 'childRegistration']);

    Route::get('/profile/login-as-{id}',
        [\App\Http\Controllers\LoginController::class, 'loginAsChild'])
        ->name('login-as-child');

    Route::get('/profile/become-teacher',[UserController::class, 'becomeTeacher'])->name('become-teacher');

    Route::get('/profile/become-parent',[UserController::class, 'becomeParent'])->name('become-parent');

    Route::post('/add-review/{id}',[\App\Http\Controllers\TeacherReviewController::class,'store'])
        ->name('add-review');

    Route::get('/delete-review/{id}', [\App\Http\Controllers\TeacherReviewController::class, 'destroy'])
        ->name('delete-review');

    Route::post('/update-review/{id}', [\App\Http\Controllers\TeacherReviewController::class, 'update'])
        ->name('update-review');

    Route::get('/open-chat-{id}', [\App\Http\Controllers\ChatController::class, 'openChat']
    )->name('open-chat');


    Route::get('/chat/rooms',
        [\App\Http\Controllers\ChatController::class, 'rooms']
    )->name('chat-rooms');

    Route::get('/chat/{id}/messages', [\App\Http\Controllers\ChatController::class, 'messages']);

    Route::get('/chat/{id}',[\App\Http\Controllers\ChatController::class, 'index']
    )->name('chat-room');


    Route::post('/chat/{id}/messages',[\App\Http\Controllers\ChatController::class, 'store']
    );

    Route::get('/chat/', [\App\Http\Controllers\ChatController::class, 'index']
    )->name('chat');

});




Route::get('/search', [\App\Http\Controllers\SearchController::class, 'show'])->name('search');

Route::get('/teachers/{id}', [\App\Http\Controllers\TeacherPageController::class,'index'])->name('user-page');



