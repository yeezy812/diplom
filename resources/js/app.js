require('./bootstrap');
import {createApp} from "vue";
import App from "@/components/App";
import SelectSubject from "@/components/SelectSubject";
import TeacherReview from "@/components/TeacherReview";
import NumberComponent from "@/components/NumberComponent";
import ChatList from "@/components/ChatList";
const app = createApp({})

app.component('firstApp', App)
app.component('selectSubject', SelectSubject)
app.component('teacherReview', TeacherReview)
app.component('numberComponent', NumberComponent)
app.component('chatList', ChatList)
app
    .mount('#app');
