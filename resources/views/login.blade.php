
@extends('layouts.app')

@section('title')
   Вход
@endsection



@section('content')
    <p class="text-center h2"> Вход</p>

    <form method="post" action="/login" class="form-control w-50  container-fluid" >
        @csrf
        @if(session('error'))
            <p class=" alert-danger"> {{session('error')}}</p>
     @endif
        <!-- Email input -->
        <div class="form-outline mb-4">
            <input class="form-control" id="email" name="email" type="text" placeholder="Email">
        </div>

        <!-- Password input -->
        <div class="form-outline mb-4">
            <input class="form-control" id="password" name="password" type="password" value="" placeholder="Пароль" autocomplete="new-password">
        </div>

        <!-- 2 column grid layout for inline styling -->



        <!-- Submit button -->
        <button type="submit" class="btn btn-primary btn-block mb-4">Войти</button>
    </form>
@endsection
