@extends('layouts.app')

@section('title')
    Поиск репетитора
@endsection

@section('search-page')
    <div class="container-fluid">
        <form class="row" method="get" action="{{route('search')}}">
            <div class="col-2 bg-white">
                <div class="row">

                    <label for="education">Образование</label>
                    <select name="education" class="form-select">
                        <option value="">не выбрано</option>
                        @foreach( \App\Models\Education::get() as $education)
                            <option value="{{$education->id}}"
                                    @if( isset($request["education"]) && $education->id == $request["education"])
                                        selected
                                        @endif
                            >  {{$education-> name}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="row">
                    <div class="row">
                        <label>Цена</label>
                        <label for="min_price">от</label>

                        <number-component :prop="{{ $request['min_price'] ?? 0}}"
                                          :name="'min_price'"
                        ></number-component>
                    </div>
                    <div class="row">
                        <label for="max_price">до</label>

                        <number-component :prop="{{ $request['max_price'] ?? 0}}"
                                          :name="'max_price'"
                        ></number-component>
                    </div>
                </div>
                <div class="row">
                    <div class="row">
                        <label>Опыт работы</label>
                        <label for="min_work_experience">от</label>
{{--                        <input class="input-group"  name="min_work_experience" placeholder="от"--}}
{{--                               @isset($request['min_work_experience'])--}}
{{--                                   value="{{$request['min_work_experience']}}"--}}
{{--                            @endisset--}}
{{--                        >--}}
                        <number-component :prop="{{ $request['min_work_experience'] ?? 0}}"
                                          :name="'min_work_experience'"
                        ></number-component>
                    </div>
{{--                    <div class="row">--}}
{{--                        <label for="max_work_experience">до</label>--}}
{{--                        <input class="input-group-sm" placeholder="до" name="max_work_experience">--}}

{{--                    </div>--}}
                </div>
                <first-app :categories="{{\App\Models\Category::with('subjects')->get()}}"
                          @isset($request['subject']) :subject-id="{{$request['subject']}}" @endisset
                           @isset($request['category'])    :category-id="{{$request['category']}}" @endisset
                >
                </first-app>
                <div class="row">
                <label>Сортировать по</label>
                <select class="form-select" name="order" @isset($request['order']) value="{{$request['order']}}" @endisset>
{{--                    <option value="price_asc">возрастанию цены</option>--}}
                    <option value="created_at">дате создания</option>
                    <option value="reviews_count">количеству отзывов</option>
                    <option value="reviews_avg">оценкам отзывов</option>
                </select>
                </div>
                <button class="btn btn-success">Найти</button>
            </div>


            <div class="col-9 bg-white">
                <div class="row">
                <label for="q">Запрос</label>
                <div class="input-group mb-3">

                    <input type="text" class="form-control"  name="q" placeholder="поиск"
                           @isset($request['q'])
                               value="{{$request['q']}}"
                           @endisset
                           aria-label="Recipient's username" aria-describedby="basic-addon2">
                    </div>
                    <div class="row" style="margin-top: 10px">

                        @if($teachers->total() > 0)

                           @foreach($teachers as $teacher)
                                <div style="width: 90%; background-color: #fff; margin: 0 auto; border:1px solid; margin-bottom: 10px " class="rounded ">
                                    <div class="container row">
                                        <div class="col">
                                            <a href="{{route('user-page',['id' => $teacher->id])}}" style="color: inherit">
                                                {{$teacher->user->name}} {{$teacher->user->last_name}}</a>
                                        </div>


                                      </div>

                                    <div class="container row">
                                    <div class="col-1">

                                            <img
                                               src="{{$teacher->user->getProfilePicturePath()}}"
                                                class="img-thumbnail rounded-circle mr-3"
                                                alt=""
                                                width="75"
                                                height="75"
                                            >


                                        <div class="row">    {{$teacher->getReviewsCount()}} |
                                          ср. оценка  {{(int)$teacher->teacherReviews()->avg('score')}}
                                        </div>
                                    </div>
                                        <div class="col">

                                            <div class="row"> <span> О себе: {{$teacher->about}}</span></div>
                                            <div class="row">
                                                @foreach($teacher->getAllCategoriesAndSubjects() as $category)

                                                    Категория: {{$category['name']}}<br>
                                                Преметы:
                                                    @foreach($category['attributes'] as $subject)
                                                        {{$subject['name']}}  @if($subject['price']) {{$subject['price']}} руб/ч@endif <br>
                                                    @endforeach
                                                    <br>
                                                @endforeach

                                            </div>
                                        </div>
                                        <div class="col-1"> образование
                                            @if (isset($teacher->education->name))
                                                {{$teacher->education->name}}
                                            @endif

                                        </div>
                                    </div>
                                </div>

                            @endforeach
                               {{($teachers->links('layouts.pagination'))}}
                        @else
                            <p class="text-center h4" style="color:grey">  по вашему запросу ничего не найдено </p>


                        @endif

                    </div>
                </div>
        </div>
    </form>

</div>
@endsection
