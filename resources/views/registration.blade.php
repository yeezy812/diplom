@extends('layouts.app')

@section('title')
    Регистрация пользователя
@endsection





@section('content')
    <form class="col-10 container-fluid" method="post" action="
        @if( isset($isCreateStudent) && $isCreateStudent)
    {{route('user.student-registration')}}
    @else
    {{route('user.registration')}}
    @endif
    {{--    {{route('user.registration')}}--}}
        ">
        @csrf
        <p class="h2 text-center">Регистрация пользователя</p>

{{--        @if ($errors->any())--}}
{{--            <div class="alert alert-danger">--}}
{{--                <ul>--}}
{{--                    @foreach ($errors->all() as $error)--}}
{{--                        <li>{{ $error }}</li>--}}
{{--                    @endforeach--}}
{{--                </ul>--}}
{{--            </div>--}}
{{--        @endif--}}

        <label for="email" l> Почта:</label>
        @error('email')
        <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <input type="text" class="form-control w-75" name="email" id="email">

        <label for="password"> Пароль:</label>
        @error('password')
        <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <input type="password" class="form-control w-75" name="password" id="password" autocomplete="new-password">

        <label for="password_confirm"> Пароль еще раз:</label>
        <input type="password" class="form-control w-75" name="password_confirmation" id="password_confirm">
        @error('last_name')
        <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <label for="last_name"> Фамилия:</label>

        <input type="text" class="form-control w-75" name="last_name" id="last_name">

        <label for="name"> Имя:</label>
        @error('name')
        <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <input type="text" class="form-control w-75" name="name" id="name">

        <label for="middle_name"> Отчество:</label>
        @error('middle_name')
        <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <input type="text" class="form-control w-75" name="middle_name" id="middle_name">

        <label for="birthdate">Дата рождения</label>
        @error('birthdate')
        <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <input type="date" class="form-control w-75"  name="birthdate">

        @if( !isset($isCreateStudent))
            <label>Тип пользователя</label>
            <div class="form-check">
                <input class="form-check-input" type="radio" name="usertype" id="parent" value="parent">
                <label class="form-check-label" for="parent">Родитель </label>
            </div>
            <div class="form-check">
                <input class="form-check-input" type="radio" name="usertype" id="student" value="student" checked>
                <label class="form-check-label" for="student"> Обычный пользователь</label>
            </div>
            <div class="form-check">
                <input class="form-check-input" type="radio" name="usertype" id="teacher" value="teacher">
                <label class="form-check-label" for="teacher"> Преподаватель </label>
            </div>
        @endif
        <button class="btn btn-primary">Зарегистрировать аккаунт</button>
    </form>

@endsection
