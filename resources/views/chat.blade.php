@extends('layouts.app')

@section('title')
    Страница пользователя
@endsection



@section('content')
    <div class="container-fluid row h-100 ">
        <div class="col-3 border-end  h-100" style="overflow: auto">
{{--            @for($i=0; $i<10; $i++)--}}
{{--            @foreach($rooms as $roomItem)--}}
{{--                <a href="{{route('user.chat-room', ['id' => $roomItem->id])}}" class="text-reset text-decoration-none">--}}
{{--                <div class="bg-light mt-1 container">--}}
{{--                    <div class="row">--}}
{{--                        <span class="text-start">--}}
{{--                            {{$roomItem->anotherUser->where('user_id','!=', \Illuminate\Support\Facades\Auth::user()->id)->first()->name}}--}}
{{--                        </span>--}}
{{--                    </div>--}}
{{--                    <div class="row"> {{$roomItem->lastMessage->message}}--}}
{{--                        <span class="text-end">{{$roomItem->lastMessage->created_at}} </span>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--                </a>--}}

{{--            @endforeach--}}
{{--            @endfor--}}

                <buttton class="btn btn-light w-100">Загрузить еще</buttton>
        </div>
        <div class="col ">
            @isset($room)
            <div class="row bg-body border-bottom">

                <p class=" text-center h3">
                    {{$room->anotherUser->where('user_id','!=', \Illuminate\Support\Facades\Auth::user()->id)->first()->name}}
                    {{$room->anotherUser->where('user_id','!=', \Illuminate\Support\Facades\Auth::user()->id)->first()->last_name}}
                </p>
            </div>
            <div class="row   position-relative"
            style="height: calc(100vh - 127px)"

            >

                <div class=" flex-grow-0 py-3 px-4   "
                style="position : absolute; bottom   : 0"
                >
                    <div style="overflow: auto;  height: calc(100vh - 215px)">
                        <buttton class="btn btn-light w-100">Загрузить еще</buttton>

                        @foreach($room->messages as $message)

                            <p style="color:dodgerblue"> {{$message->user->name}} @if($message->user->isTeacher()) {{$message->user->middle_name}}@endif</p>
                            <p>{{$message->message}} </p>
                        @endforeach



                    </div>
                    <form class="input-group border-top" method="post" action="{{route('user.chat-room', ['id' =>$room->id])}}">
                        @csrf
                        <textarea type="text" class="form-control" name="message" placeholder="Введите ваше сообщение"></textarea>
                        <button class="btn btn-primary">Отправить</button>
                    </form>
                </div>
            </div>
            @else
                <div class="row h-100 ">
                <p class="text-center h3 align-middle">Выберите чат</p>
                </div>
            @endisset
        </div>

    </div>
@endsection
