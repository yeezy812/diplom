<header class="p-3 bg-dark text-white">
    <div class="container">
        <div class="d-flex flex-wrap align-items-center justify-content-center justify-content-lg-start">
            <a href="/" class="d-flex align-items-center mb-2 mb-lg-0 text-white text-decoration-none">
                <svg class="bi me-2" width="40" height="32" role="img" aria-label="Bootstrap"><use xlink:href="#bootstrap"></use></svg>
            </a>

            <ul class="nav col-12 col-lg-auto me-lg-auto mb-2 justify-content-center mb-md-0">
{{--                <li><a href="#" class="nav-link px-2 text-secondary">Форум</a></li>--}}
                <li><a href="{{route('user.chat')}}" class="nav-link px-2 text-white">Чат</a></li>
{{--                <li><a href="#" class="nav-link px-2 text-white">Личный Кабинет</a></li>--}}
                <li><a href="{{route('search')}}" class="nav-link px-2 text-white">Поиск </a></li>
                {{--                <li><a href="#" class="nav-link px-2 text-white">About</a></li>--}}
            </ul>

            <div class="text-end">
                @if (!Auth::check())
                    <a type="button" class="btn btn-outline-light me-2" href="{{route('user.login')}}" role="button">Войти</a>
                    <a type="button" class="btn btn-warning" href="{{route('user.registration')}}" role="button">Зарегистрироваться</a>
                @else
                    <a type="button" class="btn btn-warning" href="{{route('user.profile')}}" role="button">{{Auth::user()->name}} {{Auth::user()->last_name}}</a>
                    <a type="button" class="btn btn-outline-light me-2" href="{{route('user.logout')}}" role="button">Выйти</a>
                @endif
            </div>
        </div>
    </div>
</header>
