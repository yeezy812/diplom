@extends('layouts.app')

@section('title')
    Страница пользователя
@endsection



@section('content')
    <p class="h2"> {{$teacher->user->name}}   {{$teacher->user->last_name}}</p>
    <p class="h3">О себе</p>
    <p>{{$teacher->about}}</p>
<a class="btn btn-danger" href="{{route('user.open-chat', ['id' => $teacher->id])}}">Открыть чат</a><br>
    @foreach($teacher->getAllCategoriesAndSubjects() as $category)

        Категория: {{$category['name']}}<br>
        Преметы:
        @foreach($category['attributes'] as $subject)
            {{$subject['name']}}  @if($subject['price']) {{$subject['price']}} руб/ч@endif <br>
        @endforeach
        <br>
    @endforeach

    <hr>
    <p class="h3">Отзывы ({{$reviews->total()}})</p>
    @if(\Illuminate\Support\Facades\Auth::check())
        <form method="post" action="{{$route}}">

            @csrf
            <select name="score" class="form-select">
                @for($i=1; $i <=5; $i++)
                    <option @if(isset($userReview['score']) && $i == $userReview['score']) selected @endif>{{$i}}</option>
                @endfor
            </select>
            <textarea name="comment" class="form-control">@isset($userReview['comment']){{$userReview['comment']}}@endisset</textarea>
            @empty($userReview)
            <button class="btn btn-success"> Отправить отзыв</button>
            @else
                <button class="btn btn-success"> Изменить отзыв</button>
                <a class="btn btn-danger" href="{{route('user.delete-review', ['id' => $userReview['id']])}}">Удалить отзыв</a>
            @endempty

        </form>
    @else
        <div class="container alert alert-danger"><a href="{{route('user.login')}}">Авторизируйтесь</a>, чтобы оставлять отзывы
        </div>
    @endif
    <div class="container">
            @foreach($reviews as $review)
                <div class="container bg-light mb-3" style="border: 1px solid #000000; ">
                    <div class="row border-bottom">
                        <div class="col">{{$review->author->name}} {{$review->author->last_name}}</div>
                        <div class="col text-end">оценка: {{$review->score}} </div>
                    </div>
                    <div class="row"> {{$review->comment}}</div>
                </div>
            @endforeach
                {{($reviews->links('layouts.pagination'))}}
    </div>
    {{--<teacher-review :reviews="{{json_encode($reviews)}}"></teacher-review>--}}

@endsection
