@extends('layouts.app')

@section('title')
    Приватная страница
@endsection



@section('content')
    <div class="row">
        <div class="col">
            <p class="h1">
                <img
                    src="{{$user->getProfilePicturePath()}}"
                    class="img-thumbnail rounded-circle mr-3"
                    alt=""
                    width="100"
                    height="100"
                >
                {{$user->name}} {{$user->last_name}}
            </p>


            @if(Session::get('image_updated'))
                <p class="text-success">Изобажение успешно обновлено</p>
            @endif
            <p>{{$user->role()}}</p>
            <form class="card-body" action="{{route('user.update.profile-picture')}}"
                  method="post"
                  enctype="multipart/form-data"
            >
                @csrf
                <p class="h3">Фото профиля</p>
                @error('profile_image')
                <div class="alert alert-danger">{{ $message }}</div>
                @enderror
                <input type="file" name="profile_image">
                <button type="submit" class="btn btn-success">изменить фото</button>
            </form>
            <form class="container-fluid" method="post" action="{{route('user.update.passport-data')}}">
                @csrf
                <p class="h3">Персональная информация</p>
                @if(Session::get('passport-data.success'))`
                <div class="alert alert-success">{{Session::get('passport-data.success')}}</div>
                @endif

                <label for="last_name">Фамилия</label>
                @error('last_name')
                <div class="alert alert-danger">{{ $message }}</div>
                @enderror
                <input class="input-group-text w-100" id="last_name" name="last_name" value="{{$user->last_name}}">

                <label for="name">Имя</label>
                @error('name')
                <div class="alert alert-danger">{{ $message }}</div>
                @enderror
                <input class="input-group-text w-100" id="name" name="name" value="{{$user->name}}">

                <label for="middle_name">Отчество</label>
                @error('middle_name')
                <div class="alert alert-danger">{{ $message }}</div>
                @enderror
                <input class="input-group-text w-100" id="middle_name" name="middle_name"
                       value="{{$user->middle_name}}">

                <label for="birthdate">Дата рождения</label>

                @error('birthdate')
                <div class="alert alert-danger">{{ $message }}</div>
                @enderror
                <input type="date" class="form-control" value="{{$user->birthdate}}" name="birthdate">


                <button class="btn btn-secondary">Изменить</button>
                <hr>
            </form>

            @if(!$user->isStudent())
                <div class="container-fluid">
                    <p class="h3">Пареметры родителя</p>
                    @if($user->isParent())
                        @if(Session::get('createdNewUser'))`
                        <div class="alert alert-success">{{Session::get('createdNewUser')}}</div>
                        @endif
                        @forelse ($user->children as $child)
                            <p>
                                <a href="{{route('user.login-as-child',['id' => $child->id])}}">
                                    {{$child->name}} {{$child->last_name}}@if(!$loop->last),@endif
                                </a>
                            </p>

                        @empty
                            <p>У вас нет детей</p>
                        @endforelse
                        <p><a href="{{route('user.student-registration')}}">Вы можете создать аккаунт для ребенка</a>
                        </p>

                    @else
                        <a href="{{route('user.become-parent')}}">вы можете стать родителем</a>
                    @endif
                    <hr>
                </div>
            @endif
            <div class="container-fluid">
                <p class="h3">Параметры репетитора</p>
                @if($user->isTeacher())
                    <form class="w-100" method="post" action="{{route('user.update.teacher-about')}}">
                        @csrf

                        @if(Session::get('teacher.success'))`
                        <div class="alert alert-success">{{Session::get('teacher.success')}}</div>
                        @endif

                        <label for="education_id">Образование</label>
                        @error('education_id')
                        <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                        <select name="education_id" class="form-select">
                            <option value="">не выбрано</option>
                            @foreach( \App\Models\Education::get() as $education)
                                <option value="{{$education->id}}"
                                        @if($education->id == $user->teacherRole->education_id)
                                        selected
                                    @endif
                                >  {{$education-> name}}</option>
                            @endforeach
                        </select>


                        <label for="about">О себе</label>
                        @error('about')
                        <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                        <textarea class="form-control" id="about" name="about" rows="4"
                        >{{$user->teacherRole->about}}</textarea>

                        <label for="work_experience">Опыт работы</label>
                        @error('work_experience')
                        <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                        <number-component :prop="{{Auth::user()->teacherRole->work_experience}}"
                        :name="'work_experience'"
                        ></number-component>

                        <label>Направления</label> <br>
                        <div class="container-fluid ">
                                <select-subject  :start-values="{{json_encode(Auth::user()->teacherRole->subjectIds())}}"
                                                 :categories="{{\App\Models\Category::with('subjects')->get()}}"
                                                 :prices="{{json_encode(Auth::user()->teacherRole->getPrices())}}"
                                >

                                </select-subject>
                        </div>
                        <label>Ключевые слова</label>
                        <textarea class="form-control" name="keywords">{{Auth::user()->teacherRole->keywords}}</textarea>
                        <button class="btn btn-secondary">Изменить</button>
                        <hr>
                    </form>
                @else
                    <a href="{{route('user.become-teacher')}}"> Вы можете стать учителем</a>
                @endif
            </div>


            <form class="container-fluid" method="post" action="{{route('user.update.password')}}">
                @csrf
                <p class="h3">Безопасность</p>
                @if(Session::get('password.success'))
                    <div class="alert alert-success">{{Session::get('password.success')}}</div>
                @endif
                <label for="current_password">текущий пароль</label>
                @error('current_password')
                <div class="alert alert-danger">{{ $message}}</div>
                @enderror
                <input class="input-group-text w-100" id="current_password" name="current_password" type="password" autocomplete="new-password">

                <label for="new_password">новый пароль</label>
                @error('new_password')
                <div class="alert alert-danger">{{ $message }}</div>
                @enderror
                <input class="input-group-text w-100" id="new_password" name="new_password" type="password" autocomplete="new-password">

                <label for="new_password_confirmation">новый пароль еще раз</label>
                <input class="input-group-text w-100" id="new_password_confirmation"
                       name="new_password_confirmation"
                       autocomplete="new-password"
                       type="password">
                <button class="btn btn-secondary">Изменить</button>
                <hr>
            </form>
            <form class="container-fluid" method="post" action="{{route('user.update.email')}}">
                @csrf
                @error('email')
                <div class="alert alert-danger">{{ $message }}</div>
                @enderror
                @if(Session::get('email.success'))
                    <div class="alert alert-success">{{Session::get('email.success')}}</div>
                @endif
                <p class="h3">Email</p>
                <label for="email">email</label>
                <input class="input-group-text w-100" id="email" name="email" value="{{$user->email}}">
                <button class="btn btn-secondary">Изменить</button>
                <hr>
            </form>
        </div>
    </div>
@endsection

