<!DOCTYPE html>
<html>
<head>
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>@yield('title')</title>
</head>
<body>
<div id="app">

@include('include.header')

@yield('search-page')

<div class="bg-white container-fluid w-75" style="margin-top: 15px; min-height: calc(100vh - 100px)">
    @yield('content')
</div>


</div>
<script src="{{mix('js/app.js')}}"></script>
</body>

</html>
